module.exports = function( grunt ) {

  grunt.initConfig({
    uglify : {
      options : {
        mangle : false
      },

      my_target : {
        files : {
          'build/assets/js/scripts.min.js' : [ 'dev/scripts/*.js']
        }
      }
    },
    csslint: {
      strict: {
        options: {
          import: 2
        },
        src: ['dev/css/*.css','!dev/css/bootstrap.css']
      },
      lax: {
        options: {
          import: false
        },
        src: ['dev/css/*.css', '!dev/css/bootstrap.css']
      }
    },
    cssmin: {
      target: {
        files: {
          'build/assets/css/style.min.css': ['dev/css/*.css']
        }
      }
    },
    copy: {
      main: {
        files:[
          {expand: true, cwd: 'dev/images', src: '**', dest: 'build/assets/images/',},
          {expand: true, cwd: 'dev/scripts/ckeditor', src: '**', dest: 'build/assets/ckeditor/'}
        ]
      }
    },
    watch: {
      options: {
          livereload: true
      },
      js: {
          files: "dev/scripts/*.js",
          tasks: ["uglify"]
      },
      css: {
          files: "dev/css/*.css",
          tasks: ["autoprefixer", "cssmin"]
      },
      html: {
          files: "/*.html"
      }
    },
    connect: {
      server: {
          options: {
              port: 9000,
              base: ".",
              hostname: "localhost",
              livereload: true,
              open: true
          }
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 8', 'ie 9']
      },
      my_target: {
        files: "dev/css/*.css"
      },
    }
  });


  // Plugins do Grunt
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-autoprefixer');

  // Tarefas que serão executadas
  grunt.registerTask( 'default', ['uglify', 'cssmin', 'copy' ] );
  grunt.registerTask( 'copy_files', [ 'copy' ] );
  grunt.registerTask( "w", [ "connect", "watch" ]);

};
