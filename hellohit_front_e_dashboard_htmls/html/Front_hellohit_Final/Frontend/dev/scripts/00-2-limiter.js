(function($) {
    $.fn.extend( {
        limiter: function() {
            var elem = $($(this).data('counter-box'));
            $(this).on("keyup focus", function() {
                setCount(this);
            });
            function setCount(src) {
                var limit = src.dataset.limitChar;
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);
