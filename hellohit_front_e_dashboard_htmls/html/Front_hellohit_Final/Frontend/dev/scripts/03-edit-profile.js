$(document).ready(function(){
	/*$('.workplaces-list .item, .sponsors-list .item').click(function(){
		window.location = $(this).find('a').attr('href');
	});*/

	if($('#profile-edit-description').length > 0){
      initEditor('profile-edit-description');
  }

	$("#btn-change-cover").click(function(){
		$("#user-cover-file:hidden").trigger("click");
		return false;
	});

	$("#btn-change-avatar").click(function(){
		$("#user-avatar-file:hidden").trigger("click");
		return false;
	});

	$(".btn-load-more-content").click(function(){
		var button = $(this);
		var container = button.data('container');
		var btnText = button.text();

    $.ajax({
      url: $(this).attr('href'),
      dataType: 'html',
      success: function(data) {
				$(container).append(data);
      },
      beforeSend: function(){
        button.text('Carregando...');
      },
      complete: function(){
        button.text(btnText);
      }
    });
    return false;
  });

});

function deleteConnect(el){
  var confirmDelete = confirm("Do you want to delete the connected device?");
  if (confirmDelete == true) {
    $(el).parent().remove();
  }
  return false;
}

function deleteSponsor(el){
  var confirmDelete = confirm("Do you want to delete the sponsor?");
  if (confirmDelete == true) {
    $(el).parent().remove();
  }
  return false;
}
