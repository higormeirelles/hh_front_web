$(document).ready(function(){

	$('.open-filters').click(function(){
		if($('.pro-business-menu').hasClass('active')){
			$('.pro-business-menu').removeClass('active');
		}else{
				$('.pro-business-menu').addClass('active');
		}
		return false;
	});

	$('.pro-business-menu .sub-menu-item a').click(function(){
    if($(this).hasClass('active')){
        $(this).next('form').slideUp();
        $(this).removeClass('active');
    }else{
      $('.pro-business-menu .sub-menu-item form').slideUp();
      $('.pro-business-menu .sub-menu-item a').removeClass('active');
      $(this).addClass('active');
      $(this).next('form').slideDown();
    }

    return false;
	});

});
