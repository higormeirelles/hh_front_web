$(function() {
  if($('#editor-bio').length > 0){
    initEditor('editor-bio');
  }

  if($('#editor-project').length > 0){
    initEditor('editor-project');
  }

  if($('#currency').length > 0){
    $('#currency').maskMoney();
  }

  $('#item_for_sale').click(function(){
    if(this.checked){
      $('#item_for_sale_fields').css('display','block');
    }else{
      $('#item_for_sale_fields').css('display','none');
    }
  });

  $('#hitshot_add_video').click(function(){
    $('#type_hitshot_image').css('display','none');
    $('#type_hitshot_video').css('display','block');
  });

  $('#hitshot_add_image').click(function(){
    $('#type_hitshot_image').css('display','block');
    $('#type_hitshot_video').css('display','none');
  });

  if($('#hitshot_title').length > 0){
    $("#hitshot_title").limiter();
  }
  if($('#hitshot_description').length > 0){
    $("#hitshot_description").limiter();
  }

  

})
