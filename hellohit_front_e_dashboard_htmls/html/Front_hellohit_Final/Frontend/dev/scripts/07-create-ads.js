$(function() {
  if($('#create_ad_title').length > 0){
    $("#create_ad_title").limiter();
  }

  if($('#create_ad_description').length > 0){
    $("#create_ad_description").limiter();
  }

  $("#create_ad_title").on("keyup", function() {
      var text = $(this).val();
      if(text)
        $('#preview_ad_title').text(text);
      else
        $('#preview_ad_title').text("Advertisign Title");
  });

  $("#create_ad_description").on("keyup", function() {
      var text = $(this).val();
      if(text)
        $('#preview_ad_description').text(text);
      else
        $('#preview_ad_description').text("Advertising description limited...");
  });

  $("#create_ad_url").on("keyup", function() {
      var text = $(this).val();
      if(text)
        $('#preview_ad_title, #preview_ad_url').attr('href',text);
      else
        $('#preview_ad_title, #preview_ad_url').attr('href','#');
  });

  $("#ad_image").change(function(){
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview_ad_img').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    }
  });

});
