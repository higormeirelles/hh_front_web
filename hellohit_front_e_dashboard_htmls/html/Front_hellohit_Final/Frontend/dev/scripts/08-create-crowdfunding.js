$(document).ready(function() {

  $('#field-category-name').click(function(){
    $('#field-category-list').toggle();
    return false;
  });

  $('#field-category-list .label').click(function(){
    var val = $(this).text();
    $('#field-category-name').text(val);
    $('#field-category-name').css('color','#333333');
    $('#input-field-category-name').val(val);
    $('#field-category-list').toggle();
    return false;
  });

  if($("#crowdfunding_datepicker").length > 0){
    $("#crowdfunding_datepicker").datepicker({minDate: 0});
  }

  if($('#project_title').length > 0){
    $("#project_title").limiter();
  }

  if($('#project_blurb').length > 0){
    $("#project_blurb").limiter();
  }

  $("#project_title").on("keyup", function() {
      var text = $(this).val();
      if(text)
        $('#preview_project_title').text(text);
      else
        $('#preview_project_title').text("Project Title");
  });

  $("#project_blurb").on("keyup", function() {
      var text = $(this).val();
      if(text)
        $('#preview_project_description').text(text);
      else
        $('#preview_project_description').text("Project description limited...");
  });

  $("#project_image").change(function(){
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview_project_img').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    }
  });

  if($('#project_description').length > 0){
      initEditor('project_description');
  }

  if($('#list_rewards_copy').length > 0){
    genListRewards();
  }

  $('#create-new-reward').click(function(){
    var rewardCount = $('.reward-item').length;
    $('#reward-item-base .reward_number').text('Reward #'+rewardCount);
    var newContent = $('#reward-item-base').html();
    $('.reward-list').append(newContent);
    genListRewards();
    return false;
  });

  $('#copy-reward').click(function(){
    var copy_id = $("#list_rewards_copy option:selected").val();
    var cloned = $('.reward-item').eq(copy_id).clone();
    var reward_description = $('.reward-item').eq(copy_id).find('.reward_description').val();
    var project_amount = $('.reward-item').eq(copy_id).find('.project_amount').val();
    var project_limit_quantity = $('.reward-item').eq(copy_id).find('.project_limit_quantity').val();
    var delivery_month = $('.reward-item').eq(copy_id).find('.delivery_month').val();
    var delivery_year = $('.reward-item').eq(copy_id).find('.delivery_year').val();

    cloned.find('.reward_description').val(reward_description);
    cloned.find('.project_amount').val(project_amount);
    cloned.find('.project_limit_quantity').val(project_limit_quantity);
    cloned.find('.delivery_month').val(delivery_month);
    cloned.find('.delivery_year').val(delivery_year);

    $('.reward-list').append(cloned);
    genListRewards();
    return false;
  });

});

function deleteReward(el){
  var confirmDelete = confirm("Do you want to delete the reward?");
  if (confirmDelete == true) {
    $(el).parent().parent().remove();
    genListRewards();
  }

  return false;
}

function genListRewards(){
  var countReward = $('.reward-item').length;
  var selectOptions = "";
  for(i = 1; i < countReward; i++){
    $('.reward_number').eq(i-1).text('Reward #'+i);
    selectOptions += "<option value='"+(i-1)+"'>"+$('.reward-item').eq(i-1).find('.reward_number').text()+"</option>";
  }
  $('#list_rewards_copy').html(selectOptions);
}
