$(document).ready(function() {
  if($('.hh-talent-star-votes .stars-container').length > 0){
    //Documentation: https://github.com/irfan/jquery-star-rating

    $('.hh-talent-star-votes .stars-container').rating(function(vote, event){
      // we have vote and event variables now, lets send vote to server.
      /*$.ajax({
        url: "/get_votes.php",
        type: "GET",
        data: {rate: vote},
      });*/
    });
  }

  $('.talent-view-video').click(function(){
    var video_url = video_parser($(this).attr('href'));
    $("#embed-video").attr('src',video_url);
    return false;
  });
});

function youtube_parser(url){

}

function video_parser(url){
  var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
  var match = url.match(regExp);

  if (match){
    var embed = 'https://player.vimeo.com/video/'+match[2];
    return embed;
  }else{
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    match = url.match(regExp);
    if(match && match[7].length == 11){
      return 'https://www.youtube.com/embed/' + match[7];
    }else{
      return false;
    }
  }
}
