$(document).ready(function(){
  if($(".datepicker").length > 0){
      $(".datepicker").datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '1910:2016',
      maxDate: '+80Y'
    });
  }
});
