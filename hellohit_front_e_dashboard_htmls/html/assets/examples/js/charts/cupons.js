/*!
 * remark v1.0.7 (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';
  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();
  });
  
  // Example C3 Stacked Bar
  // ----------------------
  (function() {
    var stacked_bar_chart = c3.generate({
      bindto: '#xxxx',
      data: {  x: 'x',
    columns: [
      ['x', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
          ['data1', -30, 200, 300, 400, -150, 250],
          ['data2', 130, 100, -400, 100, -150, 50],
          ['data3', -230, 200, 200, -300, 250, 250]
        ],
        type: 'bar',
        groups: [
          ['data1', 'data2']
        ]
      },
      color: {
        pattern: [$.colors("primary", 500), $.colors("blue-grey", 300), $.colors("purple", 500), $.colors("light-green", 500)]
      },
      bar: {
        width: 45
      },
      grid: {
        y: {
          show: true,
          lines: [{
            value: 0
          }]
        }
      }
    });

    setTimeout(function() {
      stacked_bar_chart.groups([
        ['data1', 'data2', 'data3']
      ]);
    }, 1000);

    setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['data4', 100, -250, 150, 200, -300, -100]
        ]
      });
    }, 1500);

    setTimeout(function() {
      stacked_bar_chart.groups([
        ['data1', 'data2', 'data3', 'data4']
      ]);
    }, 2000);
  })();
  
  
  
    // com meses
  // ----------------------
  (function() {
    var stacked_bar_chart = c3.generate({
      bindto: '#cuponsativos',
      data: {
    x: 'x',
    columns: [
      ['x', 'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR', 'RJ', 'RN', 'RO', 'RS', 'RR', 'SC', 'SE', 'SP', 'TO'],
          ['Anúncios (Perfis Ativos)', 30, 20, 30, 40, 15, 25, 7, 22, 2, 10, 5, 8, 47, 20, 94, 49, 95, 32, 31, 56, 31, 36, 32, 65, 87, 95, 43],
        ],
		
        type: 'bar',
		labels: false,
        groups: [
          ['Anúncios (Perfis Ativos)', 'Cupons Ativos', 'Cupons Inativos']
        ],
		        order: 'null' // stack order by sum of values descendantly. this is default.
//      order: 'asc'  // stack order by sum of values ascendantly.
//      order: null   // stack order by data definition.
      },
	  axis: {
    x: {
      type: 'categorized',
    }
  },
      color: {
        pattern: ['#CADFB1', '#F8E59B', '#E4EAEC']
    },
      bar: {
        width: 24
      },
      grid: {
        y: {
          show: true,
          lines: [{
            value: 0
          }]
        }
      }
    });
	
		setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Cupons Ativos', 30, 20, 30, 40, 15, 25, 7, 22, 2, 10, 5, 8, 47, 20, 94, 49, 95, 32, 31, 56, 31, 36, 32, 65, 87, 95, 43]
        ]
      });
    }, 400);
	
	
	setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Cupons Inativos', 30, 20, 30, 40, 15, 25, 7, 22, 2, 10, 5, 8, 47, 20, 94, 49, 95, 32, 31, 56, 31, 36, 32, 65, 87, 95, 43]
        ]
      });
    }, 800);
	

  
  })();
  
  
  // Grafico de barra pagina cupons
  // ----------------------
  (function() {
    var stacked_bar_chart = c3.generate({
      bindto: '#cuponsativoscategoria',
      data: {
    x: 'x',
    columns: [
      ['x', ' '],
          ['Hospedagem', 30],
        ],
		order: 'null',
        type: 'bar',
		types: {
            Inadimplencia: 'line',
        },
		labels: false,
        groups: [
          []
        ],
		 order: 'null' // stack order by sum of values descendantly. this is default.
//      order: 'asc'  // stack order by sum of values ascendantly.
//      order: null   // stack order by data definition.
      },
	  axis: {
    x: {
      type: 'categorized',       
    }
  },
      color: {
        pattern: ['#FA9898', '#B98E7E','#77D6E1','#CCD5DB','#A2CAEE','#F7E083']
    },
      bar: {
        width: 158
      },
	  padding: {left: 0, bottom: 0},
      grid: {
        y: {
          show: true,
          lines: [{
            value: 0
          }]
        }
      }
    });
	
		setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Gastronomia', 330]
        ]
      });
    }, 400);
	
	
	setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Boates/Bares', 280]
        ]
      });
    }, 900); 
	
	setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Comércio', 145]
        ]
      });
    }, 900);
	
	
	setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Turismo', 40]
        ]
      });
    }, 900);
	
	
	setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Serviços', 654]
        ]
      });
    }, 900);

  
  })();
  
  
	setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Saúde', 200]
        ]
      });
    }, 900);
	

  
  
  // Grafico donut da pagina de tickets
  // ----------------
  (function() {
    var donut_chart = c3.generate({
				size: {
        height: 180,
		width:180
    },
      bindto: '#ticketsdonutsuporte',
      data: {
        columns: [
          ['Não Resolvidos', 30],
		  ['Aguardando', 130],
		  ['Resolvidos', 230],

        ],
        type: 'donut'
      },
      color: {
        pattern: ['#f96868', '#f2a654','#3aa99e']
    },
      legend: {
        show: false
      },
      donut: {
        label: {
          show: false
        },
        width: 20,
        title: "ultimos 30 dias",
        onclick: function(d, i) {},
        onmouseover: function(d, i) {},
        onmouseout: function(d, i) {}
      }
    });
  })();
  
                  

})(document, window, jQuery);
