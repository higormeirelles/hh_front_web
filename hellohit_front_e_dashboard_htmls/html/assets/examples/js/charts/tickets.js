/*!
 * remark v1.0.7 (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';
  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();
  });
  
  
  // Grafico de barra pagina de tickets
  // ----------------------
  (function() {
    var stacked_bar_chart = c3.generate({
      bindto: '#ticketsgraficobarra',
      data: {
    x: 'x',
    columns: [
      ['x', ' '],
          ['Não resolvidos', 30],
        ],
		order: 'null',
        type: 'bar',
		types: {
            Inadimplencia: 'line',
        },
		labels: false,
        groups: [
          []
        ]
      },
	  axis: {
    x: {
      type: 'categorized',       
    }
  },
      color: {
        pattern: ['#FA7A7A', '#F7DA64','#9ECE67']
    },
      bar: {
        width: 58
      },
	  padding: {left: 0, bottom: 0},
      grid: {
        y: {
          show: true,
          lines: [{
            value: 0
          }]
        }
      }
    });
	
		setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Aguardando', 130]
        ]
      });
    }, 400);
	
	
	setTimeout(function() {
      stacked_bar_chart.load({
        columns: [
          ['Resolvidos', 230]
        ]
      });
    }, 900); 

  
  })();
  
  
  // Grafico donut da pagina de tickets
  // ----------------
  (function() {
    var donut_chart = c3.generate({
				size: {
        height: 180,
		width:180
    },
      bindto: '#ticketsdonutsuporte',
      data: {
        columns: [
          ['Não Resolvidos', 30],
		  ['Aguardando', 130],
		  ['Resolvidos', 230],

        ],
        type: 'donut'
      },
      color: {
        pattern: ['#f96868', '#f2a654','#3aa99e']
    },
      legend: {
        show: false
      },
      donut: {
        label: {
          show: false
        },
        width: 20,
        title: "ultimos 30 dias",
        onclick: function(d, i) {},
        onmouseover: function(d, i) {},
        onmouseout: function(d, i) {}
      }
    });
  })();
  
                  

})(document, window, jQuery);
