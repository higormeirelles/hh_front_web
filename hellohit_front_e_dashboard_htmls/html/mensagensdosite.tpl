<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-icon btn-pure btn-inverse slidePanel-close actions-top icon wb-close"
    aria-hidden="true"></button>
    <div class="btn-group actions-bottom" role="group">
      <div class="pull-left" style="position:relative;">
        <button type="button" class="btn btn-icon btn-pure btn-inverse dropdown-toggle icon wb-more-horizontal"
        data-toggle="dropdown" aria-expanded="false" aria-hidden="true"></button>
        <ul class="dropdown-menu dropdown-menu-right" role="menu">
          <li><a href="javascript:void(0)"><i class="icon wb-inbox" aria-hidden="true"></i> Arquivar</a></li>
          <li><a href="javascript:void(0)"><i class="icon wb-trash" aria-hidden="true"></i> Apagar</a></li>
          <li><a href="javascript:void(0)"><i class="icon wb-print" aria-hidden="true"></i> Imprimir</a></li>
        </ul>
      </div>
      <button type="button" class="btn btn-icon btn-pure btn-inverse icon wb-chevron-left"
      aria-hidden="true"></button>
      <button type="button" class="btn btn-icon btn-pure btn-inverse icon wb-chevron-right"
      aria-hidden="true"></button>
    </div>
  </div>
  <h1>Gostaria de anunciar, como posso fazer?</h1>
</header>
<div class="slidePanel-inner">
  <section class="slidePanel-inner-section">
    <div class="mail-header">
      <div class="mail-header-main">
      <div class="avatar" style="margin-left:-40px; margin-top:-18px;">
        
         <i class="wb-large-point red-600" aria-hidden="true" style="font-size:60px;"></i>
        </div>
        <div>
          <span class="name">Bruna Garcia</span>
        </div>
        <div><a href="javascript:void(0)">brunagarcia@gmail.com</a> to <a href="javascript:void(0)">Contato do Site</a>
          
        </div>
      </div>
      <div class="mail-header-right">
        <span class="time">3 minutos atrás</span>
        <div class="btn-group actions" role="group">
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-star" aria-hidden="true"></i></button>
         
        </div>
      </div>
    </div>
    <div class="mail-content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam.
        Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper
        sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.
        Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum
        nisl eu consectetur. Integer eleifend, nisl venenatis consequat iaculis,
        lectus arcu malesuada sem, dapibus porta quam lacus eu neque.Lorem ipsum
        dolor sit amet, consectetur adipiscing elit. </p>
      <p>Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum
        ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque
        volutpat. Phasellus at ultricies neque, quis malesuada augue. Donec eleifend
        condimentum nisl eu consectetur. Integer eleifend, nisl venenatis consequat
        iaculis, lectus arcu malesuada sem, dapibus porta quam lacus eu neque.</p>
    </div>
    
  </section>
  
  <section class="slidePanel-inner-section">
  Nome: Bruna Garcia<br>
  Cidade: Rio de Janeiro / UF: RJ<br>
  Telefone: (21) 0000.0000 / Celular: (21) 0000.0000<br>
  E-mail de contato: brunagarcia@gmail.com<br>
  Empresa: Restaurante Forno Bom
  </section>
  
  
  <section class="slidePanel-inner-section">
Obs: Staff, suas respostas deverão ser encaminhadas utilizando sua conta de email externa.
  </section>
 
</div>