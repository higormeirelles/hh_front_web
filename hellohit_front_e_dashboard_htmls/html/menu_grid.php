<div class="site-gridmenu">
  <div>
    <div>
      <ul>
        <li> <a href="admin_invoice_paid.html"> <i class="icon fa-money"></i> <span>Financeiro</span> </a> </li>
        <li> <a href="admin_tickets_sales.html"> <i class="icon fa-life-ring"></i> <span>Central Cliente</span> </a> </li>
        <li> <a href="admin_clients_new.html"> <i class="icon fa-briefcase"></i> <span>Clientes</span> </a> </li>
        <li> <a href="admin_dealers_approve.html"> <i class="icon fa-user"></i> <span>Afiliados</span> </a> </li>
        <li> <a href="admin_pubm_new.html"> <i class="icon fa-mobile"></i> <span>Pub. Mobile</span> </a> </li>
        <li> <a href="admin_pubw_new.html"> <i class="icon fa-desktop"></i> <span>Pub. Web</span> </a> </li>
        <li> <a href="admin_ads_new.html"> <i class="icon fa-tag"></i> <span>Anúncios</span> </a> </li>
        <li> <a href="admin_coupons_approve.html"> <i class="icon fa-ticket"></i> <span>Cupons</span> </a> </li>
        <li> <a href="admin_bussiness_approve.html"> <i class="icon fa-magnet"></i> <span>Neg. Locais</span> </a> </li>
        <li> <a href="admin_content_suggestions.html"> <i class="icon fa-edit"></i> <span>Conteúdo</span> </a> </li>
      </ul>
    </div>
  </div>
</div>
