<div class="site-menubar">
  <div class="site-menubar-body">
    <div>
      <div>
        <ul class="site-menu">
          <li class="site-menu-category" style="text-transform:capitalize;">
            <div class="center-block text-center padding-top-20">
              <div class="avatar avatar-100 margin-bottom-20 center-block"> <img src="assets/portraits/1.jpg" alt=""> </div>
              <div class="font-size-18 font-weight-medium blue-grey-300" style="margin-top:-20px;">Breno Bitencourt</div>
              <div class="font-size-12 blue-grey-500 margin-bottom-20" style="margin-top:-16px;">ID C54587</div>
            </div>
          </li>
          
          <!-- Menu Dashboard -->
          <li class="site-menu-item"> <a href="admin_index.html" data-slug="dashboard"> <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i> <span class="site-menu-title">Dashboard</span> </a> </li>
          <!-- Menu Dashboard --> 
          
          <!-- Menu Calendario -->
          <li class="site-menu-item"> <a href="admin_calendar.html" data-slug="calendar"> <i class="site-menu-icon fa-calendar" aria-hidden="true"></i> <span class="site-menu-title">Calendario</span> </a> </li>
          <!-- Menu Calendario --> 
          
          <!-- Menu Mensagens -->
          <li class="site-menu-item"> <a href="admin_mailbox.html" data-slug="calendar"> <i class="site-menu-icon fa-envelope" aria-hidden="true"></i> <span class="site-menu-title">Mailbox</span> </a> </li>
          <!-- Menu Mensagens --> 
          
          <!-- Menu Mapa-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="tables"> <i class="site-menu-icon fa-map-marker" aria-hidden="true"></i> <span class="site-menu-title">Mapa de abrangencia</span> <span class="site-menu-arrow"></span> </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_map_clients.html" data-slug="tables-basic"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title">Mapa de clientes</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_map_dealers.html" data-slug="tables-bootstrap"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title">Mapa de afiliados</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_map_users.html" data-slug="tables-floatthead"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title">Mapa de usuários</span> </a> </li>
            </ul>
          </li>
          <!-- Menu configurações --> 
          
          <!-- Menu Financeiro-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="financeiro"> <i class="site-menu-icon fa-money" aria-hidden="true"></i> <span class="site-menu-title">Financeiro</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_invoice_unpaid.html" data-slug="financeiro"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Cobranças emitidas</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_invoice_overdue.html" data-slug="financeiro"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Cobranças em atraso</span>
                <div class="site-menu-badge"> <span class="badge badgedark">22</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_invoice_paid.html" data-slug="financeiro"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Cobranças pagas</span> </a> </li>
            </ul>
          </li>
          <!-- Menu Financeiro --> 
          
          <!-- Menu NFE-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="nfe"> <i class="site-menu-icon fa-university" aria-hidden="true"></i> <span class="site-menu-title">Nota Fiscal</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_nfe_send.html" data-slug="nfe"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">NFe emitidas</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_nfe_list.html" data-slug="nfe"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">NFe lista</span> </a> </li>
            </ul>
          </li>
          <!-- Menu NFE --> 
          
          <!-- Menu Central do Cliente-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="centralcliente"> <i class="site-menu-icon fa-life-ring" aria-hidden="true"></i> <span class="site-menu-title">Central do Cliente</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_tickets_support.html" data-slug="centralcliente"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Suporte</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_tickets_billing.html" data-slug="centralcliente"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Financeiro</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_tickets_sales.html" data-slug="centralcliente"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Comercial</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_tickets_urgent.html" data-slug="centralcliente"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Reclamação</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_tickets_flags.html" data-slug="dashboard-v1"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Denúncias</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
            </ul>
          </li>
          <!-- Menu Central do Cliente --> 
          
          <!-- Menu Publicidade Mobile-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="pubmobile"> <i class="site-menu-icon fa-mobile" aria-hidden="true"></i> <span class="site-menu-title">Publicidade Mobile</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubm_new.html" data-slug="pubmobile"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Novos Pedidos</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubm_landbanners.html" data-slug="pubmobile"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">LandBanners</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubm_topbanners.html" data-slug="pubmobile"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">TopBanners</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubm_fullbanners.html" data-slug="pubmobile"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">FullBanners</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubm_halfbanners.html" data-slug="pubmobile"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">HalfBanners</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubm_feature.html" data-slug="pubmobile"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Destaque em Pesquisa</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubm_pushnotif.html" data-slug="pubmobile"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Push Notifications</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
            </ul>
          </li>
          <!-- Menu Publicidade Mobile --> 
          
          <!-- Menu Publicidade Web-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="pubweb"> <i class="site-menu-icon fa-desktop" aria-hidden="true"></i> <span class="site-menu-title">Publicidade Web</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubw_new.html" data-slug="pubweb"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Novos Pedidos</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubw_popout.html" data-slug="pubweb"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">PopOut</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubw_fullbanners.html" data-slug="pubweb"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">FullBanners</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubw_halfbanners.html" data-slug="pubweb"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">HalfBanners</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubw_services.html" data-slug="pubweb"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Serviços</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubw_feature_places.html" data-slug="pubweb"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Lugares em Destaque</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="iadmin_pubw_bannersvert.html" data-slug="pubweb"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Banners Verticais</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_pubw_bannersbox.html" data-slug="pubweb"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Banner Box</span>
                <div class="site-menu-badge"> <span class="badge  badgedark">2</span> </div>
                </a> </li>
            </ul>
          </li>
          <!-- Menu Publicidade Web --> 
          
          <!-- Menu Anúncios-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="anuncios"> <i class="site-menu-icon fa-tag" aria-hidden="true"></i> <span class="site-menu-title">Anúncios</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_ads_new.html" data-slug="anuncios"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Novos anúncios</span>
                <div class="site-menu-badge"> <span class="badge badgedark">22</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_ads_approve.html" data-slug="anuncios"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Alterações a aprovar</span>
                <div class="site-menu-badge"> <span class="badge badgedark">22</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_ads_list.html" data-slug="anuncios"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Lista de anúncios</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_ads_createnew.html" data-slug="anuncios"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Criar novo anúncio</span> </a> </li>
            </ul>
          </li>
          <!-- Menu Anúncios --> 
          
          <!-- Menu Cupons-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="cupons"> <i class="site-menu-icon fa-ticket" aria-hidden="true"></i> <span class="site-menu-title">Cupons</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_coupons_approve.html" data-slug="cupons"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Para aprovação</span>
                <div class="site-menu-badge"> <span class="badge badgedark">22</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_coupons_list.html" data-slug="cupons"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Lista de cupons</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_coupons_createnew.html" data-slug="cupons"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Criar novo cupom</span> </a> </li>
            </ul>
          </li>
          <!-- Menu Cupons --> 
          
          <!-- Menu Negócios Locais-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="negocioslocais"> <i class="site-menu-icon fa-magnet" aria-hidden="true"></i> <span class="site-menu-title">Negócios locais</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_bussiness_approve.html" data-slug="negocioslocais"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Para aprovação</span>
                <div class="site-menu-badge"> <span class="badge badgedark">22</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_bussiness_propeties.html" data-slug="negocioslocais"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Imóveis</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_bussiness_boats.html" data-slug="negocioslocais"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Barcos</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_bussiness_services.html" data-slug="negocioslocais"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Serviços</span> </a> </li>
            </ul>
          </li>
          <!-- Menu Negócios Locais --> 
          
          <!-- Menu Conteúdo-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="conteudo"> <i class="site-menu-icon fa-edit" aria-hidden="true"></i> <span class="site-menu-title">Conteúdo</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_content_suggestions.html" data-slug="conteudo"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Sugeridos (POI)</span>
                <div class="site-menu-badge"> <span class="badge badgedark">22</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_content_events.html" data-slug="conteudo"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Eventos</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_content_poi.html" data-slug="conteudo"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Pontos de interesse</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_content_tracks.html" data-slug="conteudo"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Roteiros</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_content_blogs.html" data-slug="conteudo"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Blogs</span> </a> </li>
            </ul>
          </li>
          <!-- Menu Conteúdo --> 
          
          <!-- Menu Clientes-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="clientes"> <i class="site-menu-icon fa-briefcase" aria-hidden="true"></i> <span class="site-menu-title">Clientes</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_clients_new.html" data-slug="clientes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Novos clientes</span>
                <div class="site-menu-badge"> <span class="badge badgedark">22</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_clients_off.html" data-slug="clientes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Clientes inativos</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_clients_list.html" data-slug="clientes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Clientes lista</span> </a> </li>
            </ul>
          </li>
          <!-- Menu Clientes --> 
          
          <!-- Afiliados Parceiros-->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="afiliados"> <i class="site-menu-icon fa-user" aria-hidden="true"></i> <span class="site-menu-title">Afiliados Parceiros</span>
            <div class="site-menu-badge"> <span class="badge badge-success">2</span> </div>
            </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_dealers_approve.html" data-slug="afiliados"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Novos afiliados</span>
                <div class="site-menu-badge"> <span class="badge badgedark">22</span> </div>
                </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_dealers_off.html" data-slug="afiliados"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Afialiados inativos</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_dealers_list.html" data-slug="afiliados"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Afiliados lista</span> </a> </li>
            </ul>
          </li>
          <!-- Afiliados Parceiros --> 
          
          <!-- Usuários -->
          <li class="site-menu-item"> <a href="admin_users.html" data-slug="paginternas"> <i class="site-menu-icon fa-users" aria-hidden="true"></i> <span class="site-menu-title">Usuários do Sistema</span> </a> </li>
          <!--Usuários --> 
          
          <!-- Menu relatorios -->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="configuracoes"> <i class="site-menu-icon fa-pie-chart" aria-hidden="true"></i> <span class="site-menu-title">Relatórios</span> <span class="site-menu-arrow"></span> </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="report_invoice.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório financeiro</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="report_support.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório de atendimento</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="report_clients.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório de Clientes</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="report_dealers.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório afiliados</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="report_ads.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório anúncios</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="report_pub.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório publicidade</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="report_poi.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório poi</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="report_cupons.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório cupons</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="report_users.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Relatório usuários</span> </a> </li>
            </ul>
          </li>
          <!-- Menu relatorios --> 
          
          <!-- Páginas Internas -->
          <li class="site-menu-item"> <a href="admin_pages.html" data-slug="paginternas"> <i class="site-menu-icon fa-file-text-o" aria-hidden="true"></i> <span class="site-menu-title">Páginas Internas</span> </a> </li>
          <!-- Páginas Internas --> 
          
          <!-- Menu Mensagens em Massa -->
          <li class="site-menu-item"> <a href="admin_messages.html" data-slug="mensagensmassa"> <i class="site-menu-icon fa-paper-plane" aria-hidden="true"></i> <span class="site-menu-title">Mensagens em Massa</span> </a> </li>
          <!-- Menu Mensagens em Massa --> 
          
          <!-- Menu configurações -->
          <li class="site-menu-item has-sub"> <a href="javascript:void(0)" data-slug="configuracoes"> <i class="site-menu-icon fa-cog" aria-hidden="true"></i> <span class="site-menu-title">Configurações</span> <span class="site-menu-arrow"></span> </a>
            <ul class="site-menu-sub">
              <li class="site-menu-item"> <a class="animsition-link" href="admin_conf_users.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Usuários</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_conf_emails.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Emails</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_conf_domain.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Domínios</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_conf_catsub.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Categorias e Sub</span> </a> </li>
              <li class="site-menu-item"> <a class="animsition-link" href="admin_conf_catsub.html" data-slug="configuracoes"> <i class="site-menu-icon " aria-hidden="true"></i> <span class="site-menu-title fontsub">Variáveis</span> </a> </li>
              <!-- Menu configurações -->
            </ul>
          </li>
          <!-- Menu  -->
          
          </li>
        </ul>
        <div class="site-menubar-section">
          <h5> Milestone <span class="pull-right">30%</span> </h5>
          <div class="progress progress-xs">
            <div class="progress-bar active" style="width: 30%;" role="progressbar"></div>
          </div>
          <h5> Release <span class="pull-right">60%</span> </h5>
          <div class="progress progress-xs">
            <div class="progress-bar progress-bar-warning" style="width: 60%;" role="progressbar"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="site-menubar-footer"> <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
      data-original-title="Configurações"> <span class="icon wb-settings" aria-hidden="true"></span> </a> <a href="client_lock.html" data-placement="top" data-toggle="tooltip" data-original-title="Bloquear tela"> <span class="icon wb-eye-close" aria-hidden="true"></span> </a> <a href="index.html" data-placement="top" data-toggle="tooltip" data-original-title="Sair"> <span class="icon wb-power" aria-hidden="true"></span> </a> </div>
</div>
