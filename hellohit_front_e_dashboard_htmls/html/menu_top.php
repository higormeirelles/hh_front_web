<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
      data-toggle="menubar"> <span class="sr-only">Toggle navigation</span> <span class="hamburger-bar"></span> </button>
    <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse"> <i class="icon wb-more-horizontal" aria-hidden="true"></i> </button>
    <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu"> <span class="navbar-brand-text"><img src="assets/images/wetrip-name.png" width="89" height="27" alt="" style="margin-top:-8px;"/></span> <img src="assets/images/logo-top-colapsed.png" width="20" height="29" class="navbar-brand-logo" style="margin-top:-8px;"> </div>
  </div>
  <div class="navbar-container container-fluid"> 
    <!-- Navbar Collapse -->
    <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse"> 
      <!-- Navbar Toolbar -->
      <ul class="nav navbar-toolbar">
        <li class="hidden-float" id="toggleMenubar"> <a data-toggle="menubar" href="#" role="button"> <i class="icon hamburger hamburger-arrow-left"> <span class="sr-only">Toggle menubar</span> <span class="hamburger-bar"></span> </i> </a> </li>
        <li class="hidden-xs" id="toggleFullscreen"> <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button"> <span class="sr-only">Toggle fullscreen</span> </a> </li>
      </ul>
      <!-- End Navbar Toolbar --> 
      
      <!-- Navbar Toolbar Right -->
      <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
        <li class="dropdown"> <a data-toggle="dropdown" href="javascript:void(0)" title="Notifications" aria-expanded="false"
            data-animation="scale-up" role="button"> <i class="icon fa-user font-size-18" aria-hidden="true"></i> <span class="badge badge-success up">5</span> </a>
          <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
            <li class="dropdown-menu-header" role="presentation">
              <h5>NOVOS USUÁRIOS</h5>
              <span class="label label-round label-success">5 novos</span> </li>
            <li class="list-group" role="presentation">
              <div data-role="container">
                <div data-role="content"> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/5.jpg" alt="..."> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Daniel Almeida</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 dias atrás</time>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/5.jpg" alt="..."> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Daniel Almeida</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 dia atrás</time>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/5.jpg" alt="..."> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Daniel Almeida</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">10 horas atrás</time>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/5.jpg" alt="..."> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Daniel Almeida</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">5 horas atrás</time>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/5.jpg" alt="..."> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Daniel Almeida</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 horas atrás</time>
                    </div>
                  </div>
                  </a> </div>
              </div>
            </li>
            <li class="dropdown-menu-footer" role="presentation"> <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button"> <i class="icon fa-toggle-right" aria-hidden="true"></i> </a> <a href="javascript:void(0)" role="menuitem"> Exibir os novos afiliados </a> </li>
          </ul>
        </li>
        <li class="dropdown"> <a data-toggle="dropdown" href="javascript:void(0)" title="Notifications" aria-expanded="false"
            data-animation="scale-up" role="button"> <i class="icon fa-globe font-size-18" aria-hidden="true"></i> <span class="badge badge-warning up">5</span> </a>
          <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
            <li class="dropdown-menu-header" role="presentation">
              <h5>NOVOS PONTOS SUGERIDOS</h5>
              <span class="label label-round label-danger">5 novos</span> </li>
            <li class="list-group" role="presentation">
              <div data-role="container">
                <div data-role="content"> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-body">
                      <h6 class="media-heading">Lagoa Rodrigo de Freitas (RJ)</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 dias atrás</time>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-body">
                      <h6 class="media-heading">Hipódromo da Lagoa (RJ)</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 dias atrás</time>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-body">
                      <h6 class="media-heading">Lagoa da Pampulha (MG)</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 dia atrás</time>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-body">
                      <h6 class="media-heading">Cataratas do Iguaçu (PR)</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">8 horas atrás</time>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-body">
                      <h6 class="media-heading">Cristo Redentor (RJ)</h6>
                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 horas atrás</time>
                    </div>
                  </div>
                  </a> </div>
              </div>
            </li>
            <li class="dropdown-menu-footer" role="presentation"> <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button"> <i class="icon fa-toggle-right" aria-hidden="true"></i> </a> <a href="javascript:void(0)" role="menuitem"> Exibir pontos sugeridos </a> </li>
          </ul>
        </li>
        <li class="dropdown"> <a data-toggle="dropdown" href="javascript:void(0)" title="Messages" aria-expanded="false"
            data-animation="scale-up" role="button"> <i class="icon fa-support font-size-18" aria-hidden="true"></i> <span class="badge badge-info up">3</span> </a>
          <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
            <li class="dropdown-menu-header" role="presentation">
              <h5>CENTRAL DE SUPORTE</h5>
              <span class="label label-round label-info">3 novos pedidos</span> </li>
            <li class="list-group" role="presentation">
              <div data-role="container">
                <div data-role="content"> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/2.jpg"/> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Mary Adams</h6>
                      <div class="media-meta">
                        <time datetime="2015-06-17T20:22:05+08:00">30 minutos atrás</time>
                      </div>
                      <div class="media-detail">Boleto Errado</div>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/2.jpg"/> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Mary Adams</h6>
                      <div class="media-meta">
                        <time datetime="2015-06-17T20:22:05+08:00">25 minutos atrás</time>
                      </div>
                      <div class="media-detail">Mudar foto perfil</div>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/2.jpg"/> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Mary Adams</h6>
                      <div class="media-meta">
                        <time datetime="2015-06-17T20:22:05+08:00">23 minutos atrás</time>
                      </div>
                      <div class="media-detail">Alterar dados</div>
                    </div>
                  </div>
                  </a> <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                  <div class="media">
                    <div class="media-left padding-right-10"> <span class="avatar"> <img src="assets/portraits/2.jpg"/> </span> </div>
                    <div class="media-body">
                      <h6 class="media-heading">Mary Adams</h6>
                      <div class="media-meta">
                        <time datetime="2015-06-17T20:22:05+08:00">12 minutos atrás</time>
                      </div>
                      <div class="media-detail">Alterar boleto</div>
                    </div>
                  </div>
                  </a> </div>
              </div>
            </li>
            <li class="dropdown-menu-footer" role="presentation"> <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button"> <i class="icon fa-toggle-right" aria-hidden="true"></i> </a> <a href="javascript:void(0)" role="menuitem"> Exibir Central de Suporte </a> </li>
          </ul>
        </li>
        <li class="dropdown"> <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
            data-animation="scale-up" role="button"> <span class="avatar avatar-online"> <img src="assets/portraits/5.jpg" alt="..."> <i></i> </span> </a>
          <ul class="dropdown-menu" role="menu">
            <li role="presentation"> <a href="javascript:void(0)" role="menuitem"><i class="icon wb-user" aria-hidden="true"></i> Meu Perfil</a> </li>
            <li role="presentation"> <a href="javascript:void(0)" role="menuitem"><i class="icon wb-payment" aria-hidden="true"></i> Dados de Faturamento</a> </li>
            <li role="presentation"> <a href="javascript:void(0)" role="menuitem"><i class="icon wb-settings" aria-hidden="true"></i> Configurações</a> </li>
            <li class="divider" role="presentation"></li>
            <li role="presentation"> <a href="index.html" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Sair do Sistema</a> </li>
          </ul>
        </li>
        <li id="toggleChat"> <a data-toggle="site-sidebar" href="javascript:void(0)" title="Chat" data-url="site-sidebar.tpl"> <i class="icon wb-chat" aria-hidden="true"></i> </a> </li>
      </ul>
      <!-- End Navbar Toolbar Right --> 
    </div>
    <!-- End Navbar Collapse --> 
    
    <!-- Site Navbar Seach -->
    <div class="collapse navbar-search-overlap" id="site-navbar-search">
      <form role="search">
        <div class="form-group">
          <div class="input-search"> <i class="input-search-icon wb-search" aria-hidden="true"></i>
            <input type="text" class="form-control" name="site-search" placeholder="Search...">
            <button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search"
              data-toggle="collapse" aria-label="Close"></button>
          </div>
        </div>
      </form>
    </div>
    <!-- End Site Navbar Seach --> 
  </div>
</nav>
