<!-- Create New Messages Modal -->

<div class="modal fade" id="addMailForm" aria-hidden="true" aria-labelledby="addMailForm"
  role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
        <h4 class="modal-title">Criar novo ticket</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="row pdb10">
            <div class="col-md-6">
              <select data-plugin="selectpicker">
                <option>País</option>
                <option>Brasil</option>
                <option>Espanha</option>
                <option>Estados Unidos</option>
              </select>
            </div>
            <div class="col-md-6">
              <select data-plugin="selectpicker">
                <option>Domínio</option>
                <option>AmoParaty</option>
                <option>AmoCostaVerde</option>
                <option>SeuGuiaRio</option>
              </select>
            </div>
          </div>
          <div class="row pdb10">
            <div class="col-md-6">
              <select data-plugin="selectpicker">
                <option>Cidade</option>
                <option>Mangaratiba</option>
                <option>Angra</option>
                <option>Paraty</option>
              </select>
            </div>
            <div class="col-md-6">
              <select data-plugin="selectpicker">
                <option>Cliente</option>
                <option>ID5487-Carlos Augusto</option>
                <option>ID5488-José Nobre</option>
                <option>ID5489-Dilma da Silva</option>
              </select>
            </div>
          </div>
          <div class="row pdb10">
            <div class="col-md-6">
              <select data-plugin="selectpicker">
                <option>Setor</option>
                <option selected>Suporte</option>
                <option>Financeiro</option>
                <option>Comercial</option>
              </select>
            </div>
            <div class="col-md-6">
              <select data-plugin="selectpicker">
                <option>Assunto</option>
                <option>Atendimento</option>
                <option>Troca de senha</option>
                <option>Alteração de Anúncio</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
              <div class="input-group input-group-file">
                <input type="text" class="form-control" readonly>
                <span class="input-group-btn"> <span class="btn btn-success btn-file"> <i class="icon wb-upload" aria-hidden="true"></i>
                <input type="file" name="" multiple>
                </span> </span> </div>
            </div>
          </div>
          <div class="">
            <textarea name="content" data-provide="markdown" data-iconlibrary="fa" rows="10"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer text-left">
        <button class="btn btn-primary" data-dismiss="modal" type="submit">Enviar</button>
        <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a> </div>
    </div>
  </div>
</div>
<!-- End Create New Messages Modal -->