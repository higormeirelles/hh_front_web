<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-icon btn-pure btn-inverse slidePanel-close actions-top icon wb-close"
    aria-hidden="true"></button>
    <div class="btn-group actions-bottom" role="group">
      <div class="pull-left" style="position:relative;">
        <button type="button" class="btn btn-icon btn-pure btn-inverse dropdown-toggle icon wb-more-horizontal"
        data-toggle="dropdown" aria-expanded="false" aria-hidden="true"></button>
        <ul class="dropdown-menu dropdown-menu-right" role="menu">
          <li><a href="javascript:void(0)"><i class="icon wb-inbox" aria-hidden="true"></i> Archive</a></li>
          <li><a href="javascript:void(0)"><i class="icon wb-alert-circle" aria-hidden="true"></i> Report Spam</a></li>
          <li><a href="javascript:void(0)"><i class="icon wb-trash" aria-hidden="true"></i> Delete</a></li>
          <li><a href="javascript:void(0)"><i class="icon wb-print" aria-hidden="true"></i> Print</a></li>
        </ul>
      </div>
      <button type="button" class="btn btn-icon btn-pure btn-inverse icon wb-chevron-left"
      aria-hidden="true"></button>
      <button type="button" class="btn btn-icon btn-pure btn-inverse icon wb-chevron-right"
      aria-hidden="true"></button>
    </div>
  </div>
  <h1>Troca de Senha (TS5487)<br>
    Gastronomia  / Paraty
  </h1>
</header>
<div class="slidePanel-inner">
  <section class="slidePanel-inner-section">
    <div class="mail-header">
      <div class="mail-header-main">
        <a class="avatar" href="javascript:void(0)">
          <img src="assets/portraits/2.jpg" alt="...">
        </a>
        <div>
          <span class="name">Gwendolyn (ID65487)</span>
        </div>
        <div>Enviado em 22/05/2015 para Suporte
          <span
          class="identity"><i class="wb-medium-point red-600" aria-hidden="true"></i>Não Resolvido</span>
        </div>
      </div>
      <div class="mail-header-right">
        <span class="time">3 minutos atrás</span>
        <div class="btn-group actions" role="group">
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-star" aria-hidden="true"></i></button>
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-reply" aria-hidden="true"></i></button>
        </div>
      </div>
    </div>
    <div class="mail-content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam.
        Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper
        sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.
        Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum
        nisl eu consectetur. Integer eleifend, nisl venenatis consequat iaculis,
        lectus arcu malesuada sem, dapibus porta quam lacus eu neque.Lorem ipsum
        dolor sit amet, consectetur adipiscing elit. </p>
      <p>Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum
        ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque
        volutpat. Phasellus at ultricies neque, quis malesuada augue. Donec eleifend
        condimentum nisl eu consectetur. Integer eleifend, nisl venenatis consequat
        iaculis, lectus arcu malesuada sem, dapibus porta quam lacus eu neque.</p>
    </div>
    <div class="mail-attachments">
      <p><i Class="icon wb-paperclip"></i>Anexos | <a href="javascript:void(0)">Baixar tudo</a></p>
      <ul class="list-group">
        <li class="list-group-item">
          <span class="name">Rstuvwxyz.jpg</span>
          <span class="size">(1.40M)</span>
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-download" aria-hidden="true"></i></button>
        </li>
        <li class="list-group-item">
          <span class="name">Demo.jpg</span>
          <span class="size">(1.40M)</span>
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-download" aria-hidden="true"></i></button>
        </li>
      </ul>
    </div>
  </section>
  <section class="slidePanel-inner-section">
    <div class="mail-header">
      <div class="mail-header-main"> <a class="avatar" href="javascript:void(0)"> <img src="assets/portraits/1.jpg"> </a>
        <div> <span class="name">Wetrip (Rafael Duarte-Suporte)</span> </div>
        <div>Enviado em 22/05/2015 para <span class="name">Gwendolyn (ID65487)</span> <span
          class="identity"><i class="wb-medium-point orange-600" aria-hidden="true"></i>Aguardando</span> </div>
      </div>
      <div class="mail-header-right">
        <span class="time">2 minutos atrás</span>
        <div class="btn-group actions" role="group">
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-star" aria-hidden="true"></i></button>
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-reply" aria-hidden="true"></i></button>
        </div>
      </div>
    </div>
    <div class="mail-content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam.
        Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper
        sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.</p>
    </div>
  </section>
  <section class="slidePanel-inner-section">
    <div class="mail-header">
      <div class="mail-header-main"> <a class="avatar" href="javascript:void(0)"> <img src="assets/portraits/2.jpg" alt="..."> </a>
        <div> <span class="name">Gwendolyn (ID65487)</span> </div>
        <div>Enviado em 22/05/2015 para Suporte <span
          class="identity"><i class="wb-medium-point green-600" aria-hidden="true"></i>Resolvido</span> </div>
      </div>
      <div class="mail-header-right">
        <span class="time">1 minutes ago</span>
        <div class="btn-group actions" role="group">
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-star" aria-hidden="true"></i></button>
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-reply" aria-hidden="true"></i></button>
        </div>
      </div>
    </div>
    <div class="mail-content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam.
        Aliquam sollicitudin venenatis ipsum ac feugiat. </p>
    </div>
  </section>
  <div class="slidePanel-comment">
    <textarea class="maxlength-textarea form-control mb-sm margin-bottom-20" rows="4"></textarea>
    <button class="btn btn-primary" data-dismiss="modal" type="button">Responder</button>
  </div>
</div>