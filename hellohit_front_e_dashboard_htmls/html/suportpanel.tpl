<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-icon btn-pure btn-inverse slidePanel-close actions-top icon wb-close"
    aria-hidden="true"></button>
    <div class="btn-group actions-bottom" role="group">
      <div class="pull-left" style="position:relative;">
        <button type="button" class="btn btn-icon btn-pure btn-inverse dropdown-toggle icon wb-more-horizontal"
        data-toggle="dropdown" aria-expanded="false" aria-hidden="true"></button>
        <ul class="dropdown-menu dropdown-menu-right" role="menu">
          <li><a href="javascript:void(0)"><i class="icon wb-inbox" aria-hidden="true"></i> Archive</a></li>
          <li><a href="javascript:void(0)"><i class="icon wb-alert-circle" aria-hidden="true"></i> Report Spam</a></li>
          <li><a href="javascript:void(0)"><i class="icon wb-trash" aria-hidden="true"></i> Delete</a></li>
          <li><a href="javascript:void(0)"><i class="icon wb-print" aria-hidden="true"></i> Print</a></li>
        </ul>
      </div>
      <button type="button" class="btn btn-icon btn-pure btn-inverse icon wb-chevron-left"
      aria-hidden="true"></button>
      <button type="button" class="btn btn-icon btn-pure btn-inverse icon wb-chevron-right"
      aria-hidden="true"></button>
    </div>
  </div>
  <h1>Gostaria de me tornar uma afiliada, como posso fazer, quem procurar?</h1>
</header>
<div class="slidePanel-inner">
  <section class="slidePanel-inner-section">
    <div class="mail-header">
      <div class="mail-header-main">
        <a class="avatar" href="javascript:void(0)">
          <img src="assets/portraits/2.jpg" alt="...">
        </a>
        <div>
          <span class="name">Bruna Garcia</span>
        </div>
        <div><a href="javascript:void(0)">Mazhesee@gmail.com</a> to <a href="javascript:void(0)">me</a>
          <span
          class="identity"><i class="wb-medium-point red-600" aria-hidden="true"></i>Contato do site</span>
        </div>
      </div>
      <div class="mail-header-right">
        <span class="time">3 minutos atrás</span>
        <div class="btn-group actions" role="group">
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-star" aria-hidden="true"></i></button>
          <button type="button" class="btn btn-icon btn-pure btn-default"><i class="icon wb-reply" aria-hidden="true"></i></button>
        </div>
      </div>
    </div>
    <div class="mail-content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam.
        Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper
        sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.
        Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum
        nisl eu consectetur. Integer eleifend, nisl venenatis consequat iaculis,
        lectus arcu malesuada sem, dapibus porta quam lacus eu neque.Lorem ipsum
        dolor sit amet, consectetur adipiscing elit. </p>
      <p>Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum
        ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque
        volutpat. Phasellus at ultricies neque, quis malesuada augue. Donec eleifend
        condimentum nisl eu consectetur. Integer eleifend, nisl venenatis consequat
        iaculis, lectus arcu malesuada sem, dapibus porta quam lacus eu neque.</p>
    </div>
    
  </section>
  <section class="slidePanel-inner-section">
    <div class="mail-header">
      <div class="mail-header-main">
        <a class="avatar" href="javascript:void(0)">
          <img src="assets/portraits/1.jpg" alt="...">
        </a>
        <div>
          <span class="name">Equipe Wetrip</span>
        </div>
        <div><a href="javascript:void(0)">suporte@wetrip</a> to <a href="javascript:void(0)">Mazhesee@gmail.com</a>
          <span
          class="identity"><i class="wb-medium-point red-600" aria-hidden="true"></i>Contato do site</span>
        </div>
      </div>
      <div class="mail-header-right">
        <span class="time">2 minutos atrás</span>
         
      </div>
    </div>
    <div class="mail-content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam.
        Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper
        sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat.</p>
    </div>
  </section>
  
  <div class="slidePanel-comment">
    <textarea class="maxlength-textarea form-control mb-sm margin-bottom-20" rows="4"></textarea>
    <button class="btn btn-primary" data-dismiss="modal" type="button">Responder</button>
  </div>
</div>